package cs.mad.flashcards.activities

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.adapters.FlashcardSetDetailActivityAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import org.w3c.dom.Text

class FlashcardSetDetailActivity : AppCompatActivity() {

    private lateinit var binding: FlashcardSetDetailActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard_set_detail)

        val recycler = findViewById<RecyclerView>(R.id.Recyclere)
        val abutton = findViewById<Button>(R.id.addbutton2)
        val dbutton = findViewById<Button>(R.id.deletebutton2)
        var fc = Flashcard.getHardcodedFlashcards()
        val study = findViewById<Button>(R.id.Studybutton)

        study.setOnClickListener(){
            startActivity(Intent(this,StudySetActivity::class.java))
        }



//        abutton.setOnClickListener(){
//            fc = ab()
//            recycler.adapter = FlashcardSetDetailActivityAdapter(fc)
//            recycler.layoutManager = LinearLayoutManager(this)
//            recycler.adapter?.notifyDataSetChanged()
//        }




        recycler.adapter = FlashcardSetDetailActivityAdapter(fc,this)
        recycler.layoutManager = LinearLayoutManager(this)


    }

    fun showDialog() {
        AlertDialog.Builder(this)
            .setTitle("Term")
            .setMessage("Definiton")
            .setPositiveButton("Edit") { dialogInterface: DialogInterface, i: Int ->
                showcustomDialog()
            }
            .create()
            .show()
    }

    fun showcustomDialog(){
        val term = layoutInflater.inflate(R.layout.term,null)
        val def = layoutInflater.inflate(R.layout.definition,null)
        val termedittext = term.findViewById<EditText>(R.id.customterm)
        val defedittext = def.findViewById<EditText>(R.id.customDef)
        termedittext.setText("Term")
        defedittext.setText("Definition")

        AlertDialog.Builder(this)
            .setCustomTitle(term)
            .setView(def)
            .setPositiveButton("Done"){ dialogInterface: DialogInterface, i: Int ->

            }
            .setNegativeButton("Delete"){ dialogInterface: DialogInterface, i: Int ->

            }
            .create()
            .show()


    }



//    fun ab(): List<Flashcard>{
//        val lst = mutableListOf<Flashcard>()
//        lst.addAll(Flashcard.getHardcodedFlashcards())
//        lst.add(Flashcard("term 11", "defintion 11"))
//        return lst
//
//    }

}