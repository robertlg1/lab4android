package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recycler = findViewById<RecyclerView>(R.id.Recycler)
        val button = findViewById<Button>(R.id.addbutton)
        val dbutton = findViewById<Button>(R.id.deletebutton)
        var fcs = FlashcardSet.getHardcodedFlashcardSets()


        button.setOnClickListener() {
            fcs = ab()
            recycler.adapter = FlashcardSetAdapter(fcs)
            recycler.layoutManager = GridLayoutManager(applicationContext, 2)
            recycler.adapter?.notifyDataSetChanged()
        }


        recycler.adapter = FlashcardSetAdapter(fcs)
        recycler.layoutManager = GridLayoutManager(applicationContext, 2)


        /*
            connect to views using findViewById
            setup views here - recyclerview, button
            don't forget to notify the adapter if the data set is changed
         */


    }

        fun ab(): List<FlashcardSet>{
            val lst = mutableListOf<FlashcardSet>()
            lst.addAll(FlashcardSet.getHardcodedFlashcardSets())
            lst.add(FlashcardSet("set 11"))
            return lst
        }

}



