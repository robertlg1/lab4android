package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard
import java.util.stream.IntStream

class StudySetActivity : AppCompatActivity() {
    var num = 0
    var nummiss = 0
    var numcorrect = 0
    var numcompleted = 0
    var fc = Flashcard.getHardcodedFlashcards()
    val lst = mutableListOf<Flashcard>()
    val misslist = mutableListOf<Flashcard>()
    var td: TextView? = null
    private lateinit var binding: FlashcardSetDetailActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.study_set_item)
        td = findViewById<TextView>(R.id.Term_Def)
        val missbutton = findViewById<Button>(R.id.MissButton)
        val skipbutton = findViewById<Button>(R.id.skipbutton)
        val correctbutton = findViewById<Button>(R.id.Correctbutton)
        val exitbutton = findViewById<Button>(R.id.Exit)
        val missed = findViewById<TextView>(R.id.Missed)
        val correct = findViewById<TextView>(R.id.CorrectNum)
        val completed = findViewById<TextView>(R.id.Completed)

        exitbutton.setOnClickListener(){
            finish()
        }
        lst.addAll(fc)

        td?.setText(lst.get(num).term)
        td?.setOnClickListener(){
            td?.setText(lst.get(num).definition)
        }

        skipbutton.setOnClickListener(){
            skip()
        }

        missbutton.setOnClickListener(){
            misslist.add(lst.get(num))
            back()
            td?.setText(lst.get(num).term)
            nummiss += 1
            missed.setText("Missed: " + nummiss)

        }

        correctbutton.setOnClickListener(){
            correct()
            completed.setText("Completed: " + numcompleted)
            correct.setText("Correct: " + numcorrect)
            td?.setText(lst.get(num).term)
        }



    }

    fun skip(){
        back()
        td?.setText(lst.get(num).term)
    }

    fun back(){
        var fc2 = lst.get(num)
        lst.remove(lst.get(num))
        lst.add(fc2)
    }

    fun correct(){
        numcompleted += 1
        var check = misslist.contains(lst.get(num))
        if (check){

        } else{
            numcorrect += 1
        }
        lst.remove(lst.get(num))


    }


}