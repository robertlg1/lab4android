package cs.mad.flashcards.adapters

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.service.autofill.OnClickAction
import android.view.LayoutInflater
import android.view.View
import android.view.View.inflate
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.activities.StudySetActivity
import cs.mad.flashcards.entities.Flashcard

class FlashcardSetDetailActivityAdapter(input: List<Flashcard>, activity: FlashcardSetDetailActivity) : RecyclerView.Adapter<FlashcardSetDetailActivityAdapter.ViewHolder>() {

//(input: List<Flashcard>, activity: FlashcardSetDetailActivity)
    private val myData = input.toMutableList()
    private val activity = activity
    private lateinit var binding: FlashcardSetDetailActivity


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // store view references as properties using findViewById on view
        val textview = view.findViewById<TextView>(R.id.my_text2)
        val textView2 = view.findViewById<TextView>(R.id.my_text3)
        val addbutton = view.findViewById<Button>(R.id.addbutton2)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard_setdetat, viewGroup, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textview.text = myData[position].term
        holder.textView2.text = myData[position].definition

//        holder.textview.setOnClickListener {
//            holder.itemView.context.startActivity(Intent(holder.itemView.context, StudySetActivity::class.java))
//        }



        holder.itemView.setOnClickListener {
            activity.showDialog()
        }

//        holder.textView2.setOnClickListener {
//            AlertDialog.Builder(this)
//                .setTitle("FlashCard Title")
//                .setView(LayoutInflater.inflate(R.layout.dialog_item,null))
//
//        }



    }

    override fun getItemCount(): Int {
        return myData.size
    }

    fun addfc(fc: Flashcard){
        myData.add(fc)
        notifyItemInserted(myData.lastIndex)
    }


//    fun showDialog(viewHolder: ViewHolder, position: Int){
//        val term = activity.layoutInflater.inflate(R.layout.term,null)
//        val def = activity.layoutInflater.inflate(R.layout.definition,null)
//        val termedittext = term.findViewById<EditText>(R.id.customterm)
//        val defedittext = def.findViewById<EditText>(R.id.customDef)
//        termedittext.setText(myData[position].term)
//        defedittext.setText(myData[position].definition)
//
//        AlertDialog.Builder(activity)
//            .setCustomTitle(term)
//            .setView(def)
//            .setPositiveButton("edit"){ dialogInterface: DialogInterface, i: Int ->
//                Snackbar.make(binding.root, termedittext.text.toString(),Snackbar.LENGTH_LONG).show()
//
//
//            }
//
//
//     }



}

